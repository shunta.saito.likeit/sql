SELECT category_name, SUM(item_price) AS total_price FROM item t INNER JOIN item_category t1 ON t.category_id = t1.category_id GROUP BY t.category_id ORDER BY item_price DESC;
//itemtでitemテーブルをtに変数化(?)そしてそのtをt.category_idとして使う（itemテーブルのcategory_idってこと）。
//GROUP BYでどこのcategory_idをまとめるか、は「t」or 「t1」を使ってどっちのテーブルのcategory_idをまとめるか、明確にする。
